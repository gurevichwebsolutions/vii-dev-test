import { Injectable } from '@nestjs/common';
import { TaskDto } from '../../dto/task.dto';
import { InMemoryDBService } from '@nestjs-addons/in-memory-db';

@Injectable()
export class TaskService {
  constructor(private readonly imService: InMemoryDBService<TaskDto>) {
    imService.create({
      id: '1',
      name: 'Task1',
      startTime: '2021-04-01',
      endTime: '2021-04-05',
    });
    imService.create({
      id: '2',
      name: 'Task2',
      startTime: '2021-04-06',
      endTime: '2021-04-10',
    });
    imService.create({
      id: '3',
      name: 'Task3',
      startTime: '2021-04-11',
      endTime: '2021-04-15',
    });
  }

  all = (): TaskDto[] => {
    return this.imService.getAll();
  };

  one = (id: string): TaskDto => {
    return this.imService.get(id);
  };

  add = (task: TaskDto): TaskDto => {
    return this.imService.create(task);
  };

  update = (task: TaskDto): void => {
    return this.imService.update(task);
  };

  delete = (id: string): void => {
    return this.imService.delete(id);
  };
}
