import { Module } from '@nestjs/common';
import { TaskController } from '../../controllers/task/task.controller';
import { TaskService } from '../../services/task/task.service';
import { InMemoryDBModule } from '@nestjs-addons/in-memory-db';

@Module({
  imports: [InMemoryDBModule],
  controllers: [TaskController],
  providers: [TaskService],
})
export class TaskModule {}
