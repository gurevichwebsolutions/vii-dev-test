import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { TaskService } from '../../services/task/task.service';
import { TaskDto } from '../../dto/task.dto';

@Controller('task')
export class TaskController {
  constructor(private readonly taskService: TaskService) {}

  @Get()
  all(): TaskDto[] {
    return this.taskService.all();
  }

  @Get(':id')
  one(@Param('id') id): TaskDto {
    return this.taskService.one(id);
  }

  @Post()
  add(@Body() task: TaskDto): TaskDto {
    return this.taskService.add(task);
  }

  @Put()
  update(@Body() task: TaskDto): void {
    return this.taskService.update(task);
  }

  @Delete(':id')
  delete(@Param('id') id): void {
    return this.taskService.delete(id);
  }
}
