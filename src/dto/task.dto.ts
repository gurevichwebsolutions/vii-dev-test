export class TaskDto {
  readonly id: string;
  readonly name: string;
  readonly startTime: string;
  readonly endTime: string;
}
